#!/bin/bash
set -eu

# Install Docker to Ubuntu 16.04 LTE

baseUrl=$1

#Check Docker already present in the system, if not -install it.
if ( ! which docker | grep -q '') || (whiptail --yesno --defaultno "Docker already installed. Update?" 20 60);
then 
    curl -sS $baseUrl/docker-install.sh | bash /dev/stdin
fi

#Check RabbitMQ service end update it
temp=$(mktemp)
curl -sS -o temp $baseUrl/data/rabbitmq-server.service

if (systemctl is-active rabbitmq-server.service); then
    systemctl stop rabbitmq-server.service
fi

#Create a folder for storring RabbitMQ persistent data 
if [ ! -f /data/rabbitmq ]; then
    mkdir -p /data/rabbitmq
fi
chmod -R a+rw /data/rabbitmq

#Pull latest image
docker pull pay2me/rabbitmq:latest

mv temp /etc/systemd/system/rabbitmq-server.service

systemctl daemon-reload
systemctl start rabbitmq-server.service
systemctl --no-pager status rabbitmq-server.service
systemctl enable rabbitmq-server.service

echo "Congratulations! RabbitMQ is installed."