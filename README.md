# Starting RabbitMQ on a developer machine

1. Install latest [Docker](https://www.docker.com/products/docker)
1. If you want to save a persistent data and configuration between image starts, create a folder on a local disk for containing the data.
1. Share the disk in Docker Settings.
1. Run the command:
```cmd
docker run -p 15672:15672 -p 5672:5672 -v <PATH-TO-DATA>:/data  pay2me/rabbitmq:latest
```

# Instalation Docker and RabbitMQ on Ubuntu 16.04 LTE (xenial)

Just execute this command:
```bash
url=https://bitbucket.org/pay2me/p2m.rabbitmq/raw/master/ubuntu-xenial && curl -sS $url/install.sh | sudo bash /dev/stdin $url
```

This script installs or updates Docker, and installs or updates RabbitMQ to latest pay2me/rabbitmq image.

All persistent data and configuration are stored in */data/rabbitmq* folder.

# Administration the RabbitMQ image on server

The RabbitMQ image runs as SystemD service on *rabbitmq-server.service* name. 

## Stop RabbitMQ
```bash
sudo systemctl stop rabbitmq-server.service
```

## Start RabbitMQ
```bash
sudo systemctl start rabbitmq-server.service
```